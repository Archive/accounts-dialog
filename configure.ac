AC_INIT(AccountsDialog, 0.6.1)
AM_INIT_AUTOMAKE(dist-bzip2 foreign)

GETTEXT_PACKAGE=accounts-dialog
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE,"$GETTEXT_PACKAGE",
                   [the gettext translation domain])

# Support silent build rules, requires at least automake-1.11. Enable
# by either passing --eable-silent-rules to configure or passing V=0
# to make
m4_ifdef([AM_SILENT_RULES], [AM_SILENT_RULES([no])])

AC_PROG_CC
AC_PROG_CC_C_O
PKG_PROG_PKG_CONFIG
AM_GLIB_GNU_GETTEXT
AM_PROG_LIBTOOL
IT_PROG_INTLTOOL([0.40.0])
AM_MAINTAINER_MODE

AC_PATH_PROG(GLIB_GENMARSHAL, glib-genmarshal, no)
if test x"$GLIB_GENMARSHAL" = xno; then
	  AC_MSG_ERROR([glib-genmarshal executable not found in your path - should be installed with glib])
fi
AC_SUBST(GLIB_GENMARSHAL)

AC_PATH_PROG(APG, apg, no)
if test x"$APG" = xno; then
	  AC_MSG_ERROR([apg executable not found in your path - it's necessary for password manipulation])
fi
AC_SUBST(APG)

PKG_CHECK_MODULES(GLIB, glib-2.0 gthread-2.0)
PKG_CHECK_MODULES(GIO, gio-2.0 gio-unix-2.0)
PKG_CHECK_MODULES(GDK_PIXBUF, gdk-pixbuf-2.0)
PKG_CHECK_MODULES(GTK, gtk+-3.0 >= 2.91)
PKG_CHECK_MODULES(GNOME_DESKTOP, gnome-desktop-3.0)
PKG_CHECK_MODULES(DBUS_GLIB, dbus-glib-1)
PKG_CHECK_MODULES(POLKIT, polkit-gobject-1)
PKG_CHECK_MODULES(GCONF, gconf-2.0)
PKG_CHECK_MODULES(CHEESE, gstreamer-0.10 cheese-gtk >= 2.29.90, have_cheese=yes, have_cheese=no)

if test x$have_cheese = xyes ; then
	AC_DEFINE(HAVE_CHEESE, 1, [Define to 1 to enable cheese webcam support])
fi

AC_DEFINE_UNQUOTED([ISO_CODES_PREFIX],["`$PKG_CONFIG --variable=prefix iso-codes`"],[ISO codes prefix])
ISO_CODES=iso-codes

GNOME_COMMON_INIT
GNOME_DEBUG_CHECK
GNOME_COMPILE_WARNINGS([maximum])
GNOME_CXX_WARNINGS
GNOME_MAINTAINER_MODE_DEFINES

PANELS_DIR="`$PKG_CONFIG --variable extensiondir libgnome-control-center`"
AC_SUBST(PANELS_DIR)

PKG_CHECK_MODULES(PANEL, libgnome-control-center)

AC_CONFIG_HEADER([config.h])
AC_CONFIG_FILES([
Makefile
po/Makefile.in
data/Makefile
data/icons/Makefile
src/Makefile
])
AC_OUTPUT

echo
AC_MSG_NOTICE([accounts-dialog was configured with the following options:])
if test "x$have_cheese" = "xyes"; then
	AC_MSG_NOTICE([** Cheese webcam capture support enabled])
else
	AC_MSG_NOTICE([   Cheese webcam capture support disabled])
fi
echo
